#!/bin/sh
# echo 'Atulizar versao'
versao="$(cat versao.py | sed 's/ //g' | sed 's/versao=//g')"
echo "Atulizado para a versao $(($versao + 1))"
echo "versao = $(($versao + 1))" > versao.py
git add versao.py
git commit -m 'versao'
git checkout main
echo 'Branch atual:'
echo git branch | sed -n -e 's/^\* \(.*\)/\1/p'
git merge master
git push
echo 'Projeto scripts atualizado'
git checkout master
